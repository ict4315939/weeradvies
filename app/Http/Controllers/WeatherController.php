<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class WeatherController extends Controller
{
    public function showWeather()
    {
        $apiKey = '3c25a8778ae65c362599c3d943fdf5c8';
       // $city = 'wageningen';
        $latitude = 51.966667; 
        $longitude = 5.666667; 
    
        $response = Http::get("https://api.openweathermap.org/data/2.5/weather?q=Arnhem,nl&APPID=844a6690088eb8eb5648bd840fa751d7");

    
        $data = $response->json();
      // dd($data);

    
        return view('weather', ['weatherData' => $data]);
    }
}
