<!DOCTYPE html>
<html>
<head>
    <title>Weather App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="    https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ url('css/style.css')}}">


</head>
<body>

    <!-- weer template -->
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-4 col-sm-12 col-xs-12"> 
                   <div class="card p-4">    
                    
                       <div class="d-flex">
                           <h6 class="flex-grow-1">{{ $weatherData['name'] }}, {{ $weatherData['sys']['country']  }}</h6>
                           <h6><span id="current-time">11:08</span></h6>
                       </div>
                        
                       <div class="d-flex flex-column temp mt-5 mb-3">
                           <h1 class="mb-0 font-weight-bold" id="heading">{{ $weatherData['main']['temp'] - 273.15 }}&deg;C </h1>
                           @if($weatherData['main']['temp'] < 290.15)

                           <span class="text-danger">Advies: draag een dikke jas om niet ziek te worden</span>
                           @endif

                           <span class="small grey">{{ $weatherData['weather'][0]['main'] }}</span>

                           <span class="small grey">{{ $weatherData['weather'][0]['description']  }}</span>
                       </div>
                       
                       <div class="d-flex">
                           <div class="temp-details flex-grow-1">
                                <p class="my-1">
            
                                   <span> Wind Speed: {{ $weatherData['wind']['speed']  }} m/s </span>
                                </p>

                                <p class="my-1"> 
                                   <i class="fa fa-tint mr-2" aria-hidden="true"></i>
                                   <span> Humidity: {{ $weatherData['main']['humidity']  }}%</span> 
                                </p>

                           </div>
                           
                           <div>
                            @if( $weatherData['weather'][0]['description'] == "overcast clouds")
                               <img src="{{ URL ('images/bewolkt.png')}}" width="100px">
                           @else

                             <img src="{{ URL ('images/onweer.png')}}" width="100px">
                            
                            @endif
                           </div>
                           
                           
                       </div>
                        
          
                    </div>
            </div>
        </div>


    </div>
    <script>
            function updateTime() {
                const currentTimeElement = document.getElementById("current-time");
                const now = new Date();
                const hours = now.getHours().toString().padStart(2, '0');
                const minutes = now.getMinutes().toString().padStart(2, '0');
                const timeString = `${hours}:${minutes}`;
                currentTimeElement.textContent = timeString;
            }

            window.addEventListener("load", updateTime);

            setInterval(updateTime, 60000);
      </script>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
</html>
